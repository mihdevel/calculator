import sys

if len(sys.argv) != 4:
    print('Please enter 3 arguments.')
    exit()

num_1 = int(sys.argv[1])
operation = sys.argv[2]
num_2 = int(sys.argv[3])
 
if operation == '+':
    print('{} + {} = {}'.format(num_1, num_2, num_1 + num_2))
 
elif operation == '-':
    print('{} - {} = {}'.format(num_1, num_2, num_1 - num_2))
 
elif operation == '*':
    print('{} * {} = {}'.format(num_1, num_2, num_1 * num_2))
 
elif operation == '/':
    print('{} / {} = {}'.format(num_1, num_2, num_1 / num_2))
 
else:
    print('Enter a valid operator, please run the program again.')